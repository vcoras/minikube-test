package main

import (
    "net/http"

    "github.com/gin-gonic/gin"
)

func main() {
    router := gin.Default()
    router.GET("/status", getStatus)
    router.GET("/", getIndex)

    router.Run(":9000")
}

func getIndex(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"page": "index"})
}

func getStatus(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"page":"status", "Status": "OK"})
}