
provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "simple-rest-app-cluster"
}

resource "kubernetes_namespace" "bv-test" {
  metadata {
    name = "bv-test"
  }
}

resource "kubernetes_deployment" "simple-rest-app" {
  metadata {
    name = "simple-rest-app"
    namespace = "bv-test"
    labels = {
      app = "MySimpleRESTApp"
    }
  }

  spec {
    replicas = 3 // default value

    # lifecycle {
    #   ignore_changes = [
    #     # Number of replicas is controlled by
    #     # kubernetes_horizontal_pod_autoscaler, ignore the setting in this
    #     # deployment template.
    #     spec[0].replicas,
    #   ]
    # }

    selector {
      match_labels = {
        app = "MySimpleRESTApp"
      }
    }

    template {
      metadata {
        labels = {
          app = "MySimpleRESTApp"
        }
      }

      spec {
        affinity {
          pod_anti_affinity {
            required_during_scheduling_ignored_during_execution {
              label_selector {
                match_expressions {
                  key = "app"
                  operator = "In"
                  values = ["MySimpleRESTApp"]
                }
              }
              topology_key = "MySimpleRESTApp"
            }
          }
        }
        container {
          image = "vcoras/simple-rest-app:latest"
          name  = "simple-rest-app"
          
          port {
            container_port = 9000
          }

          # Ensure the application can resist without downtime to host issues, and maintenance operations (at all times we should be able to query the /status endpoint with success).
          liveness_probe {
            http_get {
              path = "/status"
              port = 9000

              http_header {
                name  = "X-Custom-Header"
                value = "Ensuring"
              }
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }

          // Ensure we do not run the application in privileged mode on the container.
          security_context {
            privileged = false
            allow_privilege_escalation = false   
          }
        }
      }
    }
  }
}

resource "kubernetes_horizontal_pod_autoscaler" "hpa" {
  metadata {
    name      = "hpa"
    namespace = "bv-test"
  }

  spec {
    min_replicas = 3
    max_replicas = 10

    scale_target_ref {
      kind        = "Deployments"
      name        = "simple-rest-app"
    }

    behavior {
      scale_down {
        stabilization_window_seconds = 300
        select_policy                = "Min"
        policy {
          period_seconds = 120
          type           = "Pods"
          value          = 3
        }

        policy {
          period_seconds = 310
          type           = "Percent"
          value          = 30
        }
      }
      scale_up {
        stabilization_window_seconds = 600
        select_policy                = "Max"
        policy {
          period_seconds = 180
          type           = "Percent"
          value          = 70
        }
        policy {
          period_seconds = 600
          type           = "Pods"
          value          = 10
        }
      }
    }
  }
}

resource "kubernetes_service" "balancer-service" {
  metadata {
    name = "balancer-service"
    namespace = "bv-test"
  }
  spec {
    selector = {
      app = kubernetes_deployment.simple-rest-app.metadata.0.labels.app
    }

    session_affinity = "ClientIP"
    
    port {
      port      = 9000
      node_port = 30085
    }

    type = "LoadBalancer"
  }
}

// Ensure we disable ssh through kubectl executable
resource "kubernetes_cluster_role" "disable_exec" {
  metadata {
    name = "DisableExec"
  }

  rule {
    api_groups     = [""]
    resources      = ["pods/exec"]
    verbs          = ["create"]
  }
}