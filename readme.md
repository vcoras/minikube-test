# Minikube test

  A simple REST app used to demonstrate a small infrastructure created using Terraform and Kubernetes.
  

## Docker image

Available at: https://hub.docker.com/repository/docker/vcoras/simple-rest-app

## Building and running
Ensure you have terraform, minikube and kubectl installed and available.

Start minikube
```shell
minikube start --nodes 3
```

Start minikube tunnel, needed for `balancer-service`
```shell
minikube tunnel
```

Plan terraform changes
```shell
terraform plan
```

Apply terraform changes
```shell
terraform apply
```

Get the external IP of LoadBalancer service
```shell
kubectl get services balancer-service -n bv-test
```

Access the REST app
```shell
curl http://<EXTERNAL-IP>:9000/status
```
